'use strict';

const express = require('express');
const pachelbel = express();
const sqlite = require('sqlite3');
const md5 = require('md5');

const db = new sqlite.Database(':memory:', err => {
  if (err) { console.log(err); }

  /* jshint -W101 */
  db.run('CREATE TABLE IF NOT EXISTS stats (qhash TEXT PRIMARY KEY, query TEXT, fastest INTEGER, slowest INTEGER, average INTEGER, total INTEGER);');
});

const store = (text, time) => {
  const hash = md5(text);

  db.get('SELECT * FROM stats WHERE qhash = ?', [hash], (err, row) => {
    if (err) { console.log(err); }

    if (row) {
      row.fastest = Math.min(row.fastest, time);
      row.slowest = Math.max(row.slowest, time);
      row.average = (row.average * row.total + time) / (++row.total);
    } else {
      row = {
        fastest: time,
        slowest: time,
        average: time,
        total: 1
      };
    }

    db.run(
      'INSERT OR REPLACE INTO stats (qhash, query, fastest, slowest, average, total) VALUES (?, ?, ?, ?, ?, ?)',
      [hash, text, row.fastest, row.slowest, row.average, row.total]
    );
  });
};

const Query = require('pg/lib/query');

require.cache[require.resolve('pg/lib/query')].exports = new Proxy(Query, {
  construct: function (target, args, ctor) {
    const start = process.hrtime();

    target = ctor.apply(this, args);

    const fn = target.callback;

    target.callback = function () {
      const end = process.hrtime(start);

      store(target.text, end[0] * 1e9 + end[1]);

      fn.apply(target, arguments);
    };

    return target;
  }
});

const formatInterval = number => `${Math.round(number / 1e3) / 1e3}ms`;

const getThreshold = number => {
  const ms = number / 1e6;

  if (ms > exports.thresholds.worse) { return 'worse'; }
  else if (ms > exports.thresholds.bad) { return 'bad'; }
};

pachelbel.set('views', `${__dirname}/views`);
pachelbel.set('view engine', 'pug');

pachelbel.use('/', (req, res, next) => {
  const column = ['fastest', 'slowest', 'average', 'total'].find(c => c === req.query.column) || 'average';
  const dir = req.query.dir === 'asc' ? 'asc' : 'desc';

  res.locals.format = formatInterval;
  res.locals.getThreshold = getThreshold;

  db.all(`SELECT * FROM stats ORDER BY ${column} ${dir}`, (err, rows) => {
    if (err) { return next(err); }

    res.render('index', {
      stats: rows,
      sortColumn: column,
      sortDir: dir === 'asc' ? 'desc' : 'asc',
      thresholds: exports.thresholds
    });
  });
});

exports = module.exports = pachelbel;
exports.thresholds = {
  // in milliseconds
  bad: 100,
  worse: 500
};
