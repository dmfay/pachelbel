# pachelbel

pachelbel is a query performance monitor for the
[express](https://github.com/expressjs/express) framework using the
[node-pg](https://github.com/brianc/node-postgres) driver for PostgreSQL. It
works by proxying elements of the `pg` driver to store query statistics in an
in-memory SQLite database and providing a statistics dashboard in the host
application.

## Usage

pachelbel requires the `pg` module as a peer dependency. Ensure that it is
available if you are using a framework or driver wrapper such as
[MassiveJS](https://github.com/robconery/massive-js).

In your app.js, `require` pachelbel and wire it to a route. pachelbel must be
loaded _before_ `pg` in order to proxy the driver correctly.

```js
if (process.env.NODE_ENV !== 'production') {
  app.use('/pachelbel', require('pachelbel'));
}
```

Note that pachelbel is not, by default, wired into any authentication system you
may have and will therefore happily give query statistics **and prepared
statement text** to anyone and everyone unless you take your own precautions to
secure it.

## Customizing thresholds

By default, queries taking 100ms or more are considered "bad" (yellow), and
queries taking 500ms or more "worse" (red). To customize these, change
pachelbel's `thresholds` object before the `app.use` directive:

```js
pachelbel.thresholds = { bad: 200, worse: 750 };
```
